require 'rails_helper'

describe Commands::Wallets::Create do
  let(:user) { create :user }
  let(:address) { attributes_for(:wallet)[:address] }
  let(:mocked_response) { { 'status' => '1' } }

  subject { described_class.run(user: user, address: address) }

  before do
    allow_any_instance_of(Commands::Wallets::Base).to receive(:fetch_balance)
      .and_return(mocked_response)
  end

  it { is_expected.to be_success }

  it 'creates a new wallet' do
    expect { subject }.to change(Wallet, :count).by(1)
    expect(subject.result.address).to eq(address)
  end

  context 'with invalid params' do
    let(:mocked_response) { { "status": '0' } }

    it { is_expected.to_not be_success }

    it 'has errors on address' do
      expect(subject.errors.keys).to contain_exactly('address')
    end
  end
end
