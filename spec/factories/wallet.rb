FactoryBot.define do
  factory :wallet do
    address { '0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae' }

    trait :with_user do
      association :user
    end
  end

  factory :random_wallet, class: Wallet do
    association :user
    address { SecureRandom.alphanumeric(42) }
  end
end
