FactoryBot.define do
  factory :user do
    email { 'likeuwill@yahoo.com' }
    password { 'Admin1234' }

    trait :with_wallet do
      after :create do |user|
        create :wallet, user: user
      end
    end

    trait :with_random_wallets do
      after :create do |user|
        create_list(:random_wallet, 3, user: user)
      end
    end
  end
end
