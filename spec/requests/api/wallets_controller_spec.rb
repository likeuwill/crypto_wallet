require 'rails_helper'

describe WalletsController, type: :request do
  let(:user) { create :user }
  let(:address) { '0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae' }
  let(:params) { { wallets: { address: address } } }
  let(:mocked_response) { { 'status' => '1' } }

  before do
    sign_in user
  end

  describe 'POST #create' do
    subject { post wallets_path, params: params }

    before do
      allow_any_instance_of(Commands::Wallets::Base).to receive(:fetch_balance)
        .and_return(mocked_response)
    end

    it 'creates a new wallet' do
      expect { subject }.to change(Wallet, :count).by(1)

      expect(subject).to redirect_to authenticated_root_path
      expect(Wallet.find_by(address: address)).to_not be nil
    end

    context 'wallet address already exists' do
      let!(:user) { create :user, :with_wallet }

      it 'returns bad request' do
        subject
        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with invalid params' do
      let(:mocked_response) { { 'status' => '0' } }

      describe 'with missing required params' do
        let(:address) { '' }

        it 'returns bad request' do
          subject
          expect(response).to have_http_status(:bad_request)
        end
      end

      describe 'with invalid address' do
        let(:address) { 'Invalid address' }

        it 'returns bad request' do
          subject
          expect(response).to have_http_status(:bad_request)
        end
      end
    end
  end

  describe 'GET #index' do
    let(:user) { create :user, :with_random_wallets }
    let(:mocked_response) do
      {
        'status' => '1',
        'result' =>
          [
            {
              'account' => '0xdf828870459aec77d13d5fe78328c80e776ba071',
              'balance' => '10500000000000'
            },
            {
              'account' => '0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae',
              'balance' => '645173906338579898951364'
            }
          ]
      }
    end

    subject { get authenticated_root_path }

    before do
      allow_any_instance_of(Commands::Wallets::Index).to receive(:api_response)
        .and_return(mocked_response)
    end

    it 'returns the list of wallets' do
      subject
      expect(subject).to render_template(:index)
    end
  end
end
