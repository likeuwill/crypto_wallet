require 'rails_helper'

RSpec.describe Wallet, type: :model do
  let!(:wallet) { create :wallet, :with_user }

  it { should validate_presence_of(:address) }
  it { should validate_uniqueness_of(:address).scoped_to(:user_id) }

  context 'validate wallet attributes' do
    it 'is valid' do
      expect(wallet).to be_valid
    end
  end
end
