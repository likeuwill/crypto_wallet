class CreateWallets < ActiveRecord::Migration[5.2]
  def change
    create_table :wallets, id: :uuid do |t|
      t.uuid :user_id, null: false
      t.string :address, null: false

      t.timestamps
    end

    add_index :wallets, %i[user_id address], unique: true
  end
end
