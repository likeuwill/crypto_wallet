class Wallet < ApplicationRecord
  belongs_to :user

  validates :address,
            presence: true,
            uniqueness: { scope: :user_id }
end
