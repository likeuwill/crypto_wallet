class HomeController < ApplicationController
  def index
    @count = Queries::Wallets::ActiveCount.run.to_i
  end
end
