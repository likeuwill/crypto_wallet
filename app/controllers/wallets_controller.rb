class WalletsController < ApplicationController
  # GET /wallets
  def index
    addresses = ::Queries::Wallets::AddressesList.run!(user: current_user)
    outcome = ::Commands::Wallets::Index.run(addresses: addresses)

    if outcome.success?
      @wallets = outcome.result
    else
      flash.now[:error] = outcome.errors.message_list.join(',')

      @wallets = []
    end
  end

  # GET /wallets/1
  def show
    outcome = ::Commands::Wallets::Show.run(address: account_params)

    if outcome.success?
      @address = account_params
      @balance = outcome.result[:balance]
      @count = outcome.result[:transactions_count]
      @transactions = outcome.result[:transactions]
    else
      flash.now[:error] = outcome.errors.message_list.join(',')
    end
  end

  def new; end

  # POST /wallets
  def create
    outcome = ::Commands::Wallets::Create.run(user: current_user, address: wallet_params['address'])

    if outcome.success?
      redirect_to authenticated_root_path
    else
      flash.now[:error] = outcome.errors.message_list.join(',')

      render('new', status: :bad_request)
    end
  end

  # DELETE /wallets/1
  def destroy
    Wallet.find_by(address: params['format']).destroy

    redirect_to authenticated_root_path
  end

  private

  def wallet_params
    params.require(:wallets).permit(:address)
  end

  def account_params
    params.require(:account)
  end
end
