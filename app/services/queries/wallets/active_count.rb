module Queries
  module Wallets
    class ActiveCount < ::Interfaces::Query
      def execute
        addresses = Wallet.all.pluck(:address)
        api_response = Adapters::Etherscan::MultipleBalance.new(addresses).call

        if api_response['status'] == '1'
          api_response['result'].count { |v| v['balance'].to_i.positive? }
        end
      end
    end
  end
end
