module Queries
  module Wallets
    class AddressesList < ::Interfaces::Query
      attr_reader :user

      def execute
        Wallet.where(user_id: user.id).order(created_at: :desc).pluck(:address)
      end
    end
  end
end
