module Commands
  module Wallets
    class Index < Mutations::Command
      # These inputs are required
      required do
        array :addresses
      end

      def execute
        if api_response['status'] == '1'
          adjusted_wallets(api_response['result'])
        else
          add_error(:addresses, :invalid, "Can't fetch wallets")
        end
      end

      private

      def adjusted_wallets(wallets)
        wallets.inject([]) do |result, wallet|
          result << {
            account: wallet['account'],
            balance: format('%.7f', (wallet['balance'].to_f / 1_000_000_000_000_000_000))
          }
        end
      end

      def api_response
        Adapters::Etherscan::Index.new(addresses).call
      end
    end
  end
end
