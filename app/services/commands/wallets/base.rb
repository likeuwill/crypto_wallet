module Commands
  module Wallets
    class Base < Mutations::Command
      def fetch_balance(address)
        Adapters::Etherscan::Balance.new(address).call
      end
    end
  end
end
