module Commands
  module Wallets
    class Create < Base
      # These inputs are required
      required do
        model :user
        string :address
      end

      def execute
        api_response = fetch_balance(address)
        if api_response['status'] == '1'
          handle_errors { Wallet.create!(user: user, address: address) }
        else
          add_error(:address, :invalid, "Can't create wallet")
        end
      end

      private

      def handle_errors
        yield
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        add_error(:wallet, e.class.name.underscore.to_sym, e.message)
      end
    end
  end
end
