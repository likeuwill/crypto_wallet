require 'time_diff'

module Commands
  module Wallets
    class Show < Base
      # These inputs are required
      required do
        string :address
      end

      def execute
        api_response = fetch_balance(address)

        if api_response['status'] == '1'
          balance = format('%.7f', (api_response['result'].to_f / 1_000_000_000_000_000_000))

          if fetch_transactions['status'] == '1'
            transactions = adjusted_transactions(fetch_transactions['result'])
          else
            add_error(:address, :invalid, "Can't fetch transactions")
          end
        else
          add_error(:address, :invalid, "Can't fetch balance")
        end

        { balance: balance, transactions_count: transactions.size, transactions: transactions }
      end

      private

      def adjusted_transactions(transactions)
        transactions.inject([]) do |result, tran|
          result << {
            from: tran['from'],
            to: tran['to'],
            amounth: tran['value'].to_f / 1_000_000_000_000_000_000,
            fee: (tran['gasPrice'].to_f * tran['gasUsed'].to_f) / 1_000_000_000_000_000_000,
            diff: time_diff(tran['timeStamp'])
          }
        end
      end

      def time_diff(timestamp)
        past_date = Time.zone.at(timestamp.to_i).to_date
        result = Time.diff(past_date, Date.current, '%d %h')[:diff]
        "#{result} hrs ago"
      end

      def fetch_transactions
        @fetch_transactions ||= Adapters::Etherscan::Transactions.new(address).call
      end
    end
  end
end
