module Adapters
  module Etherscan
    class MultipleBalance < Base
      API_URL = 'https://api.etherscan.io/api?module=account&action=balancemulti&address='.freeze

      private

      def url
        "#{API_URL}#{@addresses.join(',')}#{middle_url}#{ENV['API_KEY_TOKEN']}"
      end

      def middle_url
        '&tag=latest&apikey='
      end
    end
  end
end
