module Adapters
  module Etherscan
    class Transactions < Base
      API_URL = 'https://api.etherscan.io/api?module=account&action=txlist&address='.freeze

      private

      def url
        "#{API_URL}#{@addresses}#{middle_url}#{ENV['API_KEY_TOKEN']}"
      end

      def middle_url
        '&startblock=0&endblock=99999999&sort=asc&apikey='
      end
    end
  end
end
