module Adapters
  module Etherscan
    class Balance < Base
      API_URL = 'https://api.etherscan.io/api?module=account&action=balance&address='.freeze

      private

      def url
        "#{API_URL}#{@addresses}&tag=latest&apikey=#{ENV['API_KEY_TOKEN']}"
      end
    end
  end
end
