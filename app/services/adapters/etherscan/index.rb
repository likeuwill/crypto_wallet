module Adapters
  module Etherscan
    class Index < Base
      API_URL = 'https://api.etherscan.io/api?module=account&action=balancemulti&address='.freeze

      private

      def url
        "#{API_URL}#{@addresses.join(',')}&tag=latest&apikey=#{ENV['API_KEY_TOKEN']}"
      end
    end
  end
end
