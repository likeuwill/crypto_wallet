module Adapters
  module Etherscan
    class Base
      def initialize(addresses)
        @addresses = addresses
      end

      def call
        api_response.parsed_response
      end

      def api_response
        HTTParty.get(url)
      end
    end
  end
end
