module Interfaces
  class Query
    class << self
      def run(*args)
        new(*args).run
      end

      def run!(*args)
        new(*args).run.to_a
      end
    end

    def initialize(**args)
      args.each do |param_name, value|
        instance_variable_set("@#{param_name}", value)
      end
    end

    def run
      @results = execute
    end

    def execute
      raise NotImplementedError
    end
  end
end
