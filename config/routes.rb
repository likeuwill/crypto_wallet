Rails.application.routes.draw do
  get 'home/index'
  devise_for :users

  devise_scope :user do
    authenticated :user do
      root to: 'wallets#index', as: :authenticated_root

      resource :wallets
    end

    unauthenticated :user do
      root to: 'home#index', as: :unauthenticated_root
    end
  end
end
